/*    1: Array pop() Method
    The javascript pop() method is used to remove the last element from an array.
    The method returns the value of the removed item.*/
 /*   var lang = ["PHP", "Java", "Python", "C"];
    lang.pop();*/
    /*output/
    // New array:  PHP, Java, Python
    lang;
    (3) ["PHP", "Java", "Python"]*/

/*    2: Array push() Method

    The js push() method is used to add a new element to the end from an array.*/
/*    var lang = ["PHP", "Python", "Java", "C"];
    lang.push("JavaScript");*/

  /*  Output

    // New array:  PHP, Python, Java, C, JavaScript*/

/*
    3: Array toString() Method
    The js toString() method is used to convert array to string
*/


/*
Objects in JavaScript*/
   /* let user = new Object(); //object constructor!
    let user = {}; //object literal!
*/
/*
   let user = {
       name:"John",
       age:30
   };*/

   /* Exercise 1
    →Write the following code, one line for each action:
    →Create an empty object product
    →Add the name property with the ’Laptop’ value
    →Add the price property with the 1200 value
    →Change the value of the price to 1000
    →Show the product’s name and price on the screen
    →Remove the property name from the object*/

/*
   let product = Object();
   product.name = "Laptop";
   product.price = 1200;
   product.price = 1000;
   alert(product.name + product.price );
   delete product.name;
   console.log(product);
*/

/*
   let user = Object();
   user.name = "John";
   user.age = 30;
   user.isAdmin = true;
   console.log(user);
   delete user.age;
   console.log(user);
*/
/*

   let user = {
      name: "John",
      age: 30,
      isAdmin: true,
   };
   let key = prompt("What do you want to know about the user?","name");
   //→Square brackets also provide a way to obtain the name property as the result of any expression
   alert(user[key]); //"John" (if entered key is "name")*/

    /*let name = "Ivan";
    let age = 30;

    let user = {
        name: name,
        age: age
    };
    alert(user.name + "@"+ user.age + "@");*/

    //→The use case of making a property from a variable with the same name is so common,
    //that there is a special property value shorthand to make it shorter:
   /* let name = "Ivan";
    let age = 30;

    let user ={
        name,
        age
    };
    alert(user.name + "@"+ user.age + "@");
*/
   /* let user = {
        name,
        age: 30
    };
    alert(user.name);*/

    //Property Existence Check
    //→Accessing a non-existing property returns undefined
    //→Thus, you can check if a property exists by comparing it to undefined:
   /*let user = {};

   alert(user.noSuchProperty === undefined); // true*/

   /*let user = {
       name: "John",
       age: 30
   };
   alert("name" in user); //true, exists
   alert("age" in user); //true, exists
   alert("blabla" in user); //false, doesn't exist*/
/*

   let user = {
       name: "John",
       age: 30,
       isAdmin: true
   };

   for (let key in user){
       // keys
       alert(key);// name, age, isAdmin
       // values for the keys
       alert(user[key]);// John, 30, true
   }
*/
    //Exercise 2
    //→We have an object storing salaries of our team:
/*
    let salaries = {
        John: 100,
        Ann: 160,
        Peter: 130
    };
    */
    //→Sum all the salaries!
        //→Should be 390 in the example above
    //→If salaries is empty, then the result must be 0

   /* let salaries = {
        John: 100,
        Ann: 160,
        Peter: 130
    };
    let sum = salaries.John + salaries.Ann + salaries.Peter;
    alert(sum);*/
   /*
    let John;
    let Ann;
    let Peter;

   let salaries = {
       John,
       Ann,
       Peter
   };
   //let sum = salaries.John + salaries.Ann + salaries.Peter;
    function Sum(){
        if (salaries.John === undefined){
            John = Ann = Peter = 0;
        }

    }
    Sum();
    let sum = salaries.John + salaries.Ann + salaries.Peter;
    alert(sum);*/

 /*  let user = {name: "John"};
   let admin = user; // copy the reference
    admin.name = "David";
    console.log(user.name);
    */
/*
    let a = {};
    let b = a;
    console.log(a == b);// true
    console.log(b === a);// true
*/
   /* let a = {};
    let b = {};// two independent objects

    console.log(a == b);// false
    console.log(b === a);// false*/

/*
   const user = {name: "John"};
   user.age = 25;
   console.log(user);
   user.name = "David";
    console.log(user);
   // →The const would give an error if we try to set user to something else, for instance:
    user =  {name: "Ivan"}; // Error (can't reassign user)
    console.log(user);*/

    /*Cloning Objects*/
    //→We can copy all the properties of user into an empty object
/*    let user = {
        name: "David",
        age:30,
        isAdmin: true
    };
    let clone = Object.assign({},user);
    // now clone is a fully independent clone
    clone.name = "Ivan";
    // changed the data in it
    console.log(user.name);
    console.log(user);
    console.log(clone);*/

/*    →There is a standard algorithm for deep cloning that handles such cases
    →You can use a working implementation of it from the JavaScript library lodash (https://lodash.com/)
    →The method is called _.cloneDeep(obj). (https://lodash.com/docs)*/

 /*   //!!!JS object - but this is the most common way developers use :

    let obj ={};
    obj.name = "messi";
    obj.year = "2018";
    obj.speak = function(){
        return "My name is " +this.name + " and this is year "+this.year;
    };
    console.log(obj.speak());

    //Copying an Object

    let newObj = obj;
    obj.year = 2019;
    console.log(newObj.year);

    // 2019
    console.log(newObj.speak());

    //Shallow Copy

    let copyObject = Object.assign({}, newObj);
    copyObject.name = "ronaldo";
    console.log(copyObject.speak());
    //My name is ronaldo and this is year 2019

    //This example is copying newObj and all its properties to copyObject.
    // You can check out that speak method will only print the new name on copyObject
    // However, this methods fails when we have nested objects in property values.
    // Those objects are still not copied and work as shared reference in both objects.
    // Look at this example

    let sourceObject = {name:"neimar", country:{name:"Brazil"}};
    let shallowCopyObj = Object.assign({}, sourceObject);
    shallowCopyObj.country.name = "India";

    console.log(sourceObject);
    //{name: "neimar", country: {name: "India"}}

    //Deep Copy
    // If you just need to copy only properties which are not functions — there is an efficient method.
    // We are moving away from Object constructor here and using another global Object in JS — JSON

    let deepCopyObj = JSON.parse(JSON.stringify(obj));
    console.log(deepCopyObj);
    //{name: "messi", year: 2019}

    //Object.create()
    // You can also create object with Object.create() function
    // this has additional flexibility that you can choose what will be prototype of your new object.

    let createObj = Object.create(obj);
    console.log(createObj);
    console.log(createObj);
    createObj.name = "Pk";
    console.log(createObj.speak());
    //My name is Pk and this is year 2019

        //In this example obj is the prototype from which createdObj is created.
    // Which means it can use properties of prototype due to inheritance.
    //That’s why we can use speak() method without declaring that in createdObj.

    //Object.entries()
    //This is a simple function which converts JS objects to an array of arrays.
    // With inner array is pair of key and value of the object.
    // Let’s checkout a self-explanatory example

    let person = {name:"Roger", age:30};
    let entries = Object.entries(person);
    console.log(entries);
    //(2) [["name", "Roger"], ["age", 30]]


    //Object.keys()
    // This function picks only keys (or property labels) of objects and returns an array
    let keys = Object.keys(person);
    console.log(keys);
    //["name", "age"]

    //Object.values()
    // This function picks only values of objects and returns an array
    let values = Object.values(person);
    console.log(values);
    //["Roger", 30]

    //Object.freeze()
    // This function freezes the object for any further changes (key or values).
    // It may not throw any error (unless you are in strict mode)
    // but there will be no effect of value change on your object.
    let frozenObject = Object.freeze(person);
    frozenObject.name = "Nadal";
    console.log(frozenObject);
    //{name: "Roger", age: 30}*/


 //Exercise 3
    //→Create a calculator object with three methods:
    //→read() prompts two values and saves them as the object properties
    //→sum() returns the sum of the saved values
    //→mul() multiplies saved values and returns the result

   /* let calculator ={
       /!* num1,
        num2,*!/
        read(){
            num1 = +prompt("Enter num1", 0);
            num2 = +prompt("Enter num2", 0);
        },
        sum(){
            return num1 + num2;

        },
        mul(){

            return num1 * num2;
        }
    };

    calculator.read();
    alert(calculator.sum());
    alert(calculator.mul());*/


   //Symbol Type
/*
    let id1 = Symbol("id");
    let id2 = Symbol("id");
    /!*console.log(id1);*!/
    console.log(id1 == id2); // false
*/

    //Hidden Properties

   /* let user = {
        name: "john"
    };
    let id = Symbol("id");
    user [id] = "ID Value";
    console.log(user[id]);
    user[id] = "Their id value";
    console.log(user[id]);
    console.log(id);*/

/*
   let id =Symbol("id");

   let user = {
       name: "John",
       [id]: 123
   };
   console.log([id]);*/

    //Constructor Functions
    function User(name){
        this.name = name;
        this.isAdmin = false;
    }

    let user = new User("Adam");
    console.log(user.name);
    console.log(user.isAdmin);

    let user2 = new User("Ann");
    console.log(user.name);



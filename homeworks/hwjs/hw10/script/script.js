document.addEventListener('DOMContentLoaded', onDOMLoaded);

function onDOMLoaded () {

    changeVisibilityOfSetPass ('setPass','firstPass');
    changeVisibilityOfSetPass ('checkPass','secondPass');
    onClickSubmitPass ();

}

function changeVisibilityOfSetPass (inputPass,iconPass) {

    const setInputPass = document.getElementById(inputPass);
    const changePassIcon = document.getElementById(iconPass);


    changePassIcon.addEventListener('click', function() {

        if (setInputPass.type === 'password') {

            setInputPass.setAttribute('type', 'text');
            changePassIcon.classList.add('fa-eye-slash');
            changePassIcon.classList.remove('fa-eye');

        } else {

            setInputPass.setAttribute('type', 'password');
            changePassIcon.classList.add('fa-eye');
            changePassIcon.classList.remove('fa-eye-slash');

        }
    });

}

function onClickSubmitPass () {

    let errorText;
    const buttonID = document.getElementById('submitPass');
    const inputBeforeErrorText = document.getElementById('ckeckPassInput');

    buttonID.addEventListener('click', function (){

        errorTextExist ();
        const passFirstEnter = document.getElementById('setPass');
        const passSecondEnter = document.getElementById('checkPass');


        if (passFirstEnter.value === passSecondEnter.value) {

            errorTextExist ();
            alert('You are welcome');

        } else {

            errorText = inputBeforeErrorText.insertAdjacentHTML('afterend', `<span id="error-style">
            Нужно ввести одинаковые значения</span>`)
        }
    })

}

function errorTextExist () {

    let check = document.getElementById('error-style');

    if (check) {
        check.remove();
    }

}

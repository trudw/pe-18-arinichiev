function filterBy(arr, type) {
    return arr.filter(item => typeof item !== type);
}

let arr = ['w1', 1, null, 'w2', 2, 3, true, 'w3', undefined, {name: "w"}, function () {
}];
console.log(filterBy(arr, 'string'));

function isPositiveInt(value) {
    return value % 1 === 0 && value >= 0;
}

function checkNumber(value) {
    if (!isPositiveInt(value) || isNaN(value) || value === '' || value === undefined) {
        let newNum = prompt('Re-enter your number', '');
        return checkNumber(newNum);
    } else {
        return value;
    }
}

function checkOperation(operation) {
    if (operation === "+" || operation === "-" || operation === "*" || operation === "/") {
        return operation;
    } else {
        let newOperation = prompt('Re-enter your operation (+ - / *)', '');
        return checkOperation(newOperation);
    }
}

function sum(value1, value2) {
    return +value1 + +value2;
}

function minus(value1, value2) {
    return +value1 - +value2;
}

function multiply(value1, value2) {
    return +value1 * +value2;
}

function division(value1, value2) {
    return +value1 / +value2;
}

function mainOperation(value1, value2, operation) {
    let result;
    if (operation === "+") {
        result = sum(value1, value2);
    } else if (operation === "-") {
        result = minus(value1, value2);
    } else if (operation === "*") {
        result = multiply(value1, value2);
    } else {
        result = division(value1, value2);
    }
    console.log(value1 + " " + operation + " " + value2 + " = " + result);
}

let firstNum = checkNumber(prompt('Enter first number', ''));
let secondNum = checkNumber(prompt('Enter second number', ''));
let operation = checkOperation(prompt('Enter operation (+ - * /)', ''));
mainOperation(firstNum, secondNum, operation);

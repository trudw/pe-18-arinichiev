document.addEventListener('DOMContentLoaded', onDocumentLoaded);

const createButtonForRemove = document.createElement('button');
const addSpan = document.createElement('span');
const topInput = document.getElementById('number');
const userForm = document.getElementById('userForm');
const validationDescriptionPlace = document.getElementById('getUserNumberFromTopBtn');

function onDocumentLoaded () {

    onFocusTopPriceInput ();
    onBlurTopPriceInput ();

}

function onFocusTopPriceInput () {

    topInput.addEventListener('focus', function () {

        topInput.style.borderColor='green';
        topInput.style.color = 'green';

    });

}

function onBlurTopPriceInput () {

    topInput.addEventListener('blur', function () {
        if (topInput.value < 0) {
            topInput.style.borderColor = 'red';
            addSpan.innerHTML = `Please enter correct price <br>`;
            addSpan.style.color= 'red';

            validationDescriptionPlace.parentNode.insertBefore(addSpan,validationDescriptionPlace);
            return;
        }

        topInput.style.borderColor = '';

        createButtonForRemove.innerHTML = '(X)';
        createButtonForRemove.innerHTML = 'X';
        createButtonForRemove.id = 'buttonForRemoveSpan';
        addSpan.innerHTML = `Текущая цена: ${topInput.value}`;
        userForm.parentNode.insertBefore(createButtonForRemove, userForm);
        userForm.parentNode.insertBefore(addSpan, userForm);

        onRemoveButtonClick();
    });

}


function onRemoveButtonClick () {

    const deleteAll = document.getElementById('buttonForRemoveSpan');

    deleteAll.addEventListener('click', function () {

        deleteAll.remove();
        addSpan.remove();
        topInput.value = '';
    });


}


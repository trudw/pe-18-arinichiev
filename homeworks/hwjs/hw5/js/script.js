function createNewUser(firstName, lastName, birthday) {
    let newUser = {};
    Object.defineProperty(newUser, 'firstName', {
        get: function () {
            return firstName;
        }
    });
    Object.defineProperty(newUser, 'lastName', {
        get: function () {
            return lastName;
        }
    });
    Object.defineProperty(newUser, 'setFirstName', {
        set: function (newFirstName) {
            firstName = newFirstName;
        }
    });
    Object.defineProperty(newUser, 'setLastName', {
        set: function (newLastName) {
            lastName = newLastName;
        }
    });
    Object.defineProperty(newUser, 'getLogin', {
        value: function () {
            return firstName[0].toLowerCase() + lastName.toLowerCase();
        }
    });
    Object.defineProperty(newUser, 'getAge', {
        value: function () {
            let today = new Date();
            let age = today.getFullYear() - birthday.getFullYear();
            let month = today.getMonth() - birthday.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthday.getDate())) {
                age--;
            }
            return age;
        }
    });
    Object.defineProperty(newUser, 'getPassword', {
        value: function () {
            return firstName[0].toUpperCase() + lastName.toLowerCase() + birthday.getFullYear();
        }
    });
    return newUser;
}

function checkString(str) {
    for (let i = 0; i < str.length; i++) {
        if (!isNaN(str[i])) {
            let newString = prompt('Re-enter your text:', '');
            return checkString(newString);
        }
    }
    return str;
}

function checkDate(str) {
    let today = new Date();
    let date = str.match(/^(\d{1,2})\.(\d{1,2})\.(\d{4})$/);
    if (date === null) {
        let newString = prompt('Re-enter your date (dd.mm.yyyy):', '');
        return checkDate(newString);
    } else if (date[3] > today.getFullYear() || date[3] < 1900) {
        let newString = prompt('dd.mm.yyyy ' + date[3] + ' ???\n(dd.mm.yyyy):', '');
        return checkDate(newString);
    }
    return new Date(date[3], date[2] - 1, date[1]);
}

let firstName = checkString(prompt('Enter your first name:', ''));
let lastName = checkString(prompt('Enter your last name:', ''));
let birthday = checkDate(prompt('Enter your date of birth (dd.mm.yyyy):', ''));

let newUser = createNewUser(firstName, lastName, birthday);

console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

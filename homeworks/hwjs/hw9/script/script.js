document.addEventListener('DOMContentLoaded', onDOMLoaded);

function onDOMLoaded () {

    setActiveClass ();

}

function activeClassRemove () {
    const tab = document.getElementsByClassName('active');
    console.log(tab);
    if (tab) {
        const styleForRemove = tab[0];

        styleForRemove.classList.toggle('active');
    }

}

function setActiveClass () {

    const clickedTab = document.getElementById('tabsUL');
    let show;

    clickedTab.addEventListener('click', function () {

        activeClassRemove ();

        if (show) {

            show.style.display = 'none';

        }

        const addClass = event.target;

        addClass.classList.add('active');

        show = document.getElementById(`${addClass.innerText}`);
        show.style.display = 'block';

        return show;
    });
}

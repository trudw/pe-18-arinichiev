(function() {
    const $loader = $(".best-images-section .loader").hide();
    const $loadBtn = $(".best-images-section .btn");
    const $container = $(".best-images-gallery");
    $container.masonry({
        percentPosition: true,
        columnWidth: ".grid-sizer",
        itemSelector: ".grid-item",
        gutter: 6
    });
    const images = [
        [
            "new-images",
            [
                "319cc6061cfd63ce46ea5be95fb80682.jpg",
                "1987401-bigthumbnail.jpg",
                "adventure-journal-songs-about-mountains.jpg",
                "blue-ridge-mountains-shutterstock_213140653.jpg",
                "download.jpeg",
                "ocean_surface.jpg",
                "pexels-photo-326055.jpeg",
                "pexels-photo-618833.jpeg",
                "photo-1518837695005-2083093ee35b.jpeg"
            ]
        ],
        [
            "web-design",
            [
                "web-design1.jpg",
                "web-design2.jpg",
                "web-design3.jpg",
                "web-design4.jpg",
                "web-design5.jpg",
                "web-design6.jpg",
                "web-design7.jpg"
            ]
        ],
        [
            "graphic-design",
            [
                "graphic-design1.jpg",
                "graphic-design2.jpg",
                "graphic-design3.jpg",
                "graphic-design4.jpg",
                "graphic-design5.jpg",
                "graphic-design6.jpg",
                "graphic-design7.jpg",
                "graphic-design8.jpg",
                "graphic-design9.jpg",
                "graphic-design10.jpg",
                "graphic-design11.jpg",
                "graphic-design12.jpg"
            ]
        ],
        [
            "landing-page",
            [
                "landing-page1.jpg",
                "landing-page2.jpg",
                "landing-page3.jpg",
                "landing-page4.jpg",
                "landing-page5.jpg",
                "landing-page6.jpg",
                "landing-page7.jpg"
            ]
        ],
        [
            "wordpress",
            [
                "wordpress1.jpg",
                "wordpress2.jpg",
                "wordpress3.jpg",
                "wordpress4.jpg",
                "wordpress5.jpg",
                "wordpress6.jpg",
                "wordpress7.jpg",
                "wordpress8.jpg",
                "wordpress9.jpg",
                "wordpress10.jpg"
            ]
        ],
        [
            "best-images",
            [
                "1.png",
                "2.png",
                "5.png",
                "6.png",
                "7.png",
                "8.png",
                "9.png",
                "10.png",
                "20.png",
                "21.png",
                "22.png",
                "23.png",
                "24.png",
                "25.png",
                "26.png"
            ]
        ],
        ["services", ["1.png", "2.jpg", "3.png", "4.jpg", "5.png", "6.png"]]
    ];
    function getImageSrc(array) {
        const folderIndex = randomIndex(array.length);
        const folder = array[folderIndex];
        const imageIndex = folder[1].splice(randomIndex(folder[1].length), 1);
        if (folder[1].length === 0) {
            array.splice(folderIndex, 1);
        }
        return `img/${folder[0]}/${imageIndex}`;
    }

    function randomIndex(length) {
        let index = Math.ceil(Math.random() * length) - 1;
        return index;
    }

    function createHoverDiv() {
        const $hoverDiv = $("<div class='best-hover-div'>)");
        const $searchIcon = $("<i class='fas fa-search'>");
        const $expandIcon = $("<i class='fas fa-expand'>");
        $hoverDiv.append($searchIcon);
        $hoverDiv.append($expandIcon);
        return $hoverDiv;
    }
    function createGalleryItem() {
        const hoverDiv = createHoverDiv();
        const imageSrc = getImageSrc(images);
        const $newGalleryItem = $("<div hidden>");

        const $newImage = $(`<img class='grid-item-img' src="${imageSrc}">`);
        $newImage.on("load", function() {
            const width = $newImage.prop("width");
            if (width <= 700) {
                $newGalleryItem.addClass("medium-size");
            } else if (width > 700) {
                $newGalleryItem.addClass("big-size");
            }
            $newGalleryItem.addClass("grid-item");
        });
        $newGalleryItem.append(hoverDiv).append($newImage);
        $container.append($newGalleryItem);
        $container.imagesLoaded(function() {
            $container.masonry("addItems", $newGalleryItem);
        });
    }

    function buttonClicked() {
        $loadBtn.hide(1);
        $loader.show(1);
        setTimeout(loadImages, 2000);
    }

    function loadImages() {
        for (let i = 0; i < 24; i++) {
            createGalleryItem();
            if (!images.length) {
                $($loadBtn).remove();
                break;
            }
        }
        $container.imagesLoaded(function() {
            $(".grid-item").removeAttr("hidden");
            $container.masonry("layout");
        });
        setTimeout(()=>{$($loadBtn).show(1);
            $loader.hide(1)},0)

    }

    loadImages();
    $loadBtn.on("click", buttonClicked);
})();
